# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170909191146) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 255
    t.text     "body",          limit: 65535
    t.integer  "resource_id",   limit: 4
    t.string   "resource_type", limit: 255
    t.integer  "author_id",     limit: 4
    t.string   "author_type",   limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "answers", force: :cascade do |t|
    t.text     "title",       limit: 65535
    t.integer  "question_id", limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "answers", ["question_id"], name: "fk_rails_3d5ed4418f", using: :btree

  create_table "candidates", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "candidates", ["email"], name: "index_candidates_on_email", unique: true, using: :btree
  add_index "candidates", ["reset_password_token"], name: "index_candidates_on_reset_password_token", unique: true, using: :btree

  create_table "candidates_exams", force: :cascade do |t|
    t.integer  "exam_id",      limit: 4
    t.integer  "candidate_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "candidates_exams", ["candidate_id"], name: "index_candidates_exams_on_candidate_id", using: :btree
  add_index "candidates_exams", ["exam_id", "candidate_id"], name: "index_candidates_exams_on_exam_id_and_candidate_id", unique: true, using: :btree
  add_index "candidates_exams", ["exam_id"], name: "index_candidates_exams_on_exam_id", using: :btree

  create_table "exams", force: :cascade do |t|
    t.string   "title",       limit: 255
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "exams_questions", force: :cascade do |t|
    t.integer  "exam_id",     limit: 4
    t.integer  "question_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "exams_questions", ["exam_id", "question_id"], name: "index_exams_questions_on_exam_id_and_question_id", unique: true, using: :btree
  add_index "exams_questions", ["exam_id"], name: "index_exams_questions_on_exam_id", using: :btree
  add_index "exams_questions", ["question_id"], name: "index_exams_questions_on_question_id", using: :btree

  create_table "model_test_answers", force: :cascade do |t|
    t.integer  "model_test_id", limit: 4
    t.integer  "question_id",   limit: 4
    t.integer  "answer_id",     limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "model_tests", force: :cascade do |t|
    t.integer  "candidate_id", limit: 4
    t.integer  "exam_id",      limit: 4
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.float    "total_marks",  limit: 24, default: 0.0
    t.boolean  "is_complete",             default: false
  end

  create_table "other_exams", force: :cascade do |t|
    t.string   "title",       limit: 255
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "other_exams_marks", force: :cascade do |t|
    t.integer  "candidate_id",  limit: 4
    t.integer  "other_exam_id", limit: 4
    t.float    "point",         limit: 24
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "other_exams_marks", ["candidate_id"], name: "fk_rails_b0431ed13a", using: :btree
  add_index "other_exams_marks", ["other_exam_id"], name: "fk_rails_43a510c58a", using: :btree

  create_table "questions", force: :cascade do |t|
    t.text     "title",             limit: 65535
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "correct_answer_id", limit: 4
  end

  add_foreign_key "answers", "questions"
  add_foreign_key "candidates_exams", "candidates"
  add_foreign_key "candidates_exams", "exams"
  add_foreign_key "exams_questions", "exams"
  add_foreign_key "exams_questions", "questions"
  add_foreign_key "other_exams_marks", "candidates"
  add_foreign_key "other_exams_marks", "other_exams"
end
