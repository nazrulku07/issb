class CreateOtherExamsMarks < ActiveRecord::Migration
  def change
    create_table :other_exams_marks do |t|
      t.references :candidate, foreign_key: true
      t.references :other_exam, foreign_key: true
      t.float :point

      t.timestamps null: false
    end
  end
end
