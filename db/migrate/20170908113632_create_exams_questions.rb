class CreateExamsQuestions < ActiveRecord::Migration
  def change
    create_table :exams_questions do |t|
      t.references :exam, foreign_key: true, index: true
      t.references :question, foreign_key: true, index: true

      t.timestamps
    end

    add_index :exams_questions, [:exam_id, :question_id], :unique => true
  end
end
