class CreateOtherExams < ActiveRecord::Migration
  def change
    create_table :other_exams do |t|
      t.string :title
      t.text :description

      t.timestamps null: false
    end
  end
end
