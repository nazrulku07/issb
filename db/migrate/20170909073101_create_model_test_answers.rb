class CreateModelTestAnswers < ActiveRecord::Migration
  def change
    create_table :model_test_answers do |t|
      t.integer :model_test_id
      t.integer :question_id
      t.integer :answer_id

      t.timestamps null: false
    end
  end
end
