class CreateCandidatesExam < ActiveRecord::Migration
  def change
    create_table :candidates_exams do |t|
      t.references :exam, foreign_key: true, index: true
      t.references :candidate, foreign_key: true, index: true

      t.timestamps
    end

    add_index :candidates_exams, [:exam_id, :candidate_id], :unique => true
  end
end
