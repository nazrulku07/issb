class CreateModelTests < ActiveRecord::Migration
  def change
    create_table :model_tests do |t|
      t.integer :candidate_id
      t.integer :exam_id

      t.timestamps null: false
    end
  end
end
