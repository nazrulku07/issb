class AddTotalMarksIsCompleteFieldToModelTests < ActiveRecord::Migration
  def change
    add_column :model_tests, :total_marks, :float, default: 0
    add_column :model_tests, :is_complete, :boolean, default: false
  end
end
