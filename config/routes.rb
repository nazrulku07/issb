Rails.application.routes.draw do

  resources :exams, only: [:index, :show] do
    get :start_exam, on: :member
  end

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  devise_for :candidates

  # You can have the root of your site routed with "root"
  root 'home#index'

  get 'secret' => 'home#secret'
  get 'about' => 'home#about'
  get 'contact' => 'home#contact'

  get '/profile', to: 'home#profile', as: :candidate_profile
  post '/test/:test_id/answer', to: 'exams#answer', as: :test_answer
  post '/test/:test_id/complete_test', to: 'exams#complete_test', as: :complete_test
end
