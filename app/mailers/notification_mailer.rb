class NotificationMailer < ApplicationMailer
  default from: "mahabubziko@example.com"
  def send_notification(user, notes)
    @user = user
    @notes = notes
    mail(to: @user.email, subject: 'Notification from Admin')
  end
end
