class ExamsController < ApplicationController
  skip_before_filter :verify_authenticity_token,only: [:answer]

  def index
   @exams = current_candidate.exams.present? ? current_candidate.exams : []
  end

  def show
    @exam = Exam.find_by_id(params[:id])
    model_test = @exam.exam_given(current_candidate)
    if model_test.present?
      p "<<<<<<<<<<<<<<<<<<<<,"
      p "here"
      p "<<<<<<<<<<<<<<<<<<<<,"
     @model_test = model_test
      @flag = true
    else
      @model_test = current_candidate.model_tests.create(exam_id: @exam.id)
    end
  end

  def start_exam
    @exam = Exam.find_by_id(params[:id])
    model_test = @exam.exam_given(current_candidate)
    if model_test.present?
      if model_test.is_complete?
        @flag = true
      else
        @model_test = model_test
        @flag = false
      end
    end
  end

  def answer
    model_test = ModelTest.find_by_id(params[:test_id])
    question = Question.find_by_id(params[:question_id])
    model_test_answer = model_test.model_test_answers.find_or_initialize_by(question_id: params[:question_id])
    model_test_answer.answer_id = params[:answer]
    model_test_answer.save
  end

  def complete_test
    count_marks = 0
    model_test = ModelTest.find_by_id(params[:test_id])
    model_test.model_test_answers.each do |test_answer|
      question =  Question.find_by_id(test_answer.question_id)
      if question.correct_answer_id == test_answer.answer_id
        count_marks =  count_marks + 1
      end
    end
    model_test.total_marks = count_marks
    model_test.is_complete = true
    model_test.save
    redirect_to candidate_profile_path
  end



end
