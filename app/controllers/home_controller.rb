class HomeController < ApplicationController
  before_action :authenticate_candidate!

  def index
  end

  def profile
    @candidate = current_candidate
  end

  def about

  end

  def contact

  end

end
