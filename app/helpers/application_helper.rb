module ApplicationHelper

  def get_result_all
    @list = []
    Candidate.all.each do |can|
      @list << {
          :email => can.email,
          :marks => can.model_tests.sum(:total_marks) +  can.other_exams_marks.sum(:point)
      }
    end
    @list = @list.sort{|a,b| b[:marks] <=> a[:marks]}
    return @list
  end
end
