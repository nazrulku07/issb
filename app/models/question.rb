class Question < ActiveRecord::Base

  has_many :answers
  has_and_belongs_to_many :exams
  belongs_to :correct_answer, class_name: 'Answer', foreign_key: :correct_answer_id

  accepts_nested_attributes_for :answers, :allow_destroy => true

end
