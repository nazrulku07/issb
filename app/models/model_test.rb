class ModelTest < ActiveRecord::Base
  belongs_to :candidate
  belongs_to :exam
  has_many :model_test_answers
end
