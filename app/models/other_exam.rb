class OtherExam < ActiveRecord::Base

  has_many :other_exams_marks

  def get_mark(candidate_id)
    exam_mark = self.other_exams_marks.where(candidate_id: candidate_id).first
    exam_mark.present? ? exam_mark.point : ''
  end
end
