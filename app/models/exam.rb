class Exam < ActiveRecord::Base

  has_and_belongs_to_many :questions
  has_many :model_tests
  has_and_belongs_to_many :candidates


  def exam_given(candidate)
   model_tests.where(candidate_id:  candidate.id).first
  end
end
