ActiveAdmin.register AdminUser do


  permit_params :email, :password, :password_confirmation

  index do
    selectable_column
    id_column
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs do
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end



  # batch_action :send_email_notification do |ids|
  #   batch_action_collection.find(ids).each do |user|
  #     NotificationMailer.send_notification(user).deliver
  #   end
  #   redirect_to admin_admin_users_path, alert: "The notification has beed sent"
  # end

  batch_action :send_email_notification , form: {
                        notes:  :textarea,
                    } do |ids, inputs|
    # inputs is a hash of all the form fields you requested
    batch_action_collection.find(ids).each do |user|
        NotificationMailer.send_notification(user, inputs[:notes]).deliver
        end
    redirect_to admin_admin_users_path, alert: "The notification has beed sent"
  end

  batch_action :send_sms_notification , form: {
                                            notes:  :textarea,
                                        } do |ids, inputs|
    # inputs is a hash of all the form fields you requested
    batch_action_collection.find(ids).each do |user|

    end
    redirect_to admin_admin_users_path, alert: "The notification has beed sent"
  end



end
