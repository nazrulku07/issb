ActiveAdmin.register OtherExamsMark do

  controller do

    def permitted_params
      params.permit!
    end
  end

  form partial: 'form'

  collection_action :addition, method: :post do
    # Do some CSV importing work here...
    p params
    exam_mark_attributes = params[:exam_mark_attributes]
    exam_mark_attributes.each do |key, other_exams_mark_params|
      p other_exams_mark_params
      p other_exams_mark_params[:point]
      mark = OtherExamsMark.where(other_exam_id: other_exams_mark_params[:other_exam_id], candidate_id: other_exams_mark_params[:candidate_id]).first
      if mark.present?
        mark.point = other_exams_mark_params[:point]
      else
        mark = OtherExamsMark.new(other_exam_id: other_exams_mark_params[:other_exam_id], candidate_id: other_exams_mark_params[:candidate_id], point: other_exams_mark_params[:point])
      end
      mark.save!
    end
    redirect_to admin_candidates_path, notice: 'Mark distributed successfully!'
  end
end
