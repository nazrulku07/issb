ActiveAdmin.register Question do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  form_lambda = lambda do
    builds = Exam.all.map do |build|
      ["#{ build.title }", build.id]
    end

    { exam: builds }
  end

  batch_action :set_exam, form: form_lambda do |ids, inputs|
    p ids
    p inputs
    exam = Exam.find_by_id(inputs[:exam].to_i)
    if exam.present?
      batch_action_collection.find(ids).each do |ques|
        begin
          exam.questions << ques
        rescue
          p 'duplicate entry'
        end
      end
      redirect_to admin_exam_path(exam), notice: 'Successful.'
    else
      redirect_to admin_questions_path, alert: 'Exam title not found.'
    end
  end

  controller do

    def permitted_params
      params.permit!
    end

  end

  form do |f|

    f.semantic_errors
    f.inputs do
      f.input :title, input_html: { :rows => 3 }
      f.has_many :answers, heading: 'Answers' do |ff|
        ff.input :title, label: false, input_html: { :rows => 1 }
      end
    end
    actions

  end

  show do

    attributes_table do
      row :title
      row :created_at
      row :updated_at
      row :answers do
        ul do
          question.answers.each do |answer|
            li(style: 'position: relative;') do
              span do
                answer.title
              end
              span do
                if question.correct_answer_id == answer.id
                  raw '&#10004;'
                else
                  link_to 'set as correct answer', set_correct_answer_admin_question_path(question, answer_id: answer.id)
                end
              end
            end
          end
        end
      end
    end

  end

  member_action :set_correct_answer do
    ques = Question.find_by_id(params[:id])
    ques.correct_answer_id = params[:answer_id]
    ques.save

    redirect_to admin_question_path(ques), :notice => 'Correct answer assigned'
  end

end
