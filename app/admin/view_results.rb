ActiveAdmin.register_page "Result" do
  content do
    panel "Result" do
      table_for @items = get_result_all do |candidate|
        column "Email", :email
        column "Marks", :marks
      end
  end
end
end