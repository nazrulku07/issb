ActiveAdmin.register Exam do

  controller do

    def permitted_params
      params.permit!
    end

  end

  show do

    attributes_table do
      row :title
      row :description
      row :created_at
      row :updated_at
      row :questions do
        ol do
          exam.questions.each do |question|
            li(style: 'padding: 5px 0px 5px 0px') do
              span do
                link_to question.title, admin_question_path(question)
              end
              span do
                link_to remove_question_admin_exam_path(exam, question_id: question.id), style: 'color: red; text-decoration: none; padding-left: 5px;', data: {confirm: 'Are you sure?'} do
                  raw '&#10006;'
                end
              end
            end
          end
        end
      end
    end

  end

  member_action :remove_question do
    exm = Exam.find_by_id(params[:id])
    exm.questions.delete(params[:question_id])

    redirect_to admin_exam_path(exm), :notice => 'Question removed from the exam.'
  end

end
