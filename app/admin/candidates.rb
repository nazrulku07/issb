ActiveAdmin.register Candidate do

  form_lambda = lambda do
    builds = Exam.all.map do |build|
      ["#{ build.title }", build.id]
    end

    { exam: builds }
  end

  batch_action :assign_for_exam, form: form_lambda do |ids, inputs|
    p ids
    p inputs
    exam = Exam.find_by_id(inputs[:exam].to_i)
    if exam.present?
      batch_action_collection.find(ids).each do |cand|
        begin
          exam.candidates << cand
        rescue
          p 'duplicate entry'
        end
      end
      redirect_to admin_candidates_path, notice: 'Successful.'
    else
      redirect_to admin_candidates_path, alert: 'Exam title not found.'
    end
  end

  controller do

    def permitted_params
      params.permit!
    end

  end

  index do

    selectable_column
    column :id
    column :email
    column :mark do |candidate|
      link_to 'Set Mark', new_admin_other_exams_mark_path(candidate_id: candidate.id)
    end
    actions

  end

  form do |f|
    f.inputs do
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

  show do
    total_mark = 0.0
    attributes_table do
      row :email
      row :created_at
      row :updated_at
    end

    panel 'Assigned IQ Exams' do
      table_for candidate.exams do
        column 'Exam Name', :title
        column 'Status'
        column 'Actions' do

        end
      end
    end

    panel 'Exam Marks' do
      render partial: 'total_marks'
    end
  end


end
